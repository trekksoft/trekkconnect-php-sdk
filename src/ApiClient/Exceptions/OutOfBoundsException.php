<?php

namespace TrekkConnect\Sdk\ApiClient\Exceptions;

class OutOfBoundsException extends Exception
{
}
