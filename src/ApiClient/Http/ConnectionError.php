<?php

namespace TrekkConnect\Sdk\ApiClient\Http;

use TrekkConnect\Sdk\ApiClient\Exceptions\RuntimeException;

class ConnectionError extends RuntimeException
{
}
