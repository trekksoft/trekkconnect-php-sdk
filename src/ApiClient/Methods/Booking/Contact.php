<?php

namespace TrekkConnect\Sdk\ApiClient\Methods\Booking;

class Contact
{
    private $fullName;
    private $email;
    private $phoneNumber;

    public function __construct($fullName = null, $email = null, $phoneNumber = null)
    {
        $this->fullName = $fullName;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
    }

    public function generate()
    {
        return [
            'fullName' => $this->fullName,
            'email' => $this->email,
            'phoneNumber' => $this->phoneNumber,
        ];
    }
}