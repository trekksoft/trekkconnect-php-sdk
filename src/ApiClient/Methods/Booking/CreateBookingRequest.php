<?php

namespace TrekkConnect\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class CreateBookingRequest
{
    private $supplierId;
    private $bookingItems;
    private $holdDurationSeconds;

    public function __construct($supplierId, $holdDurationSeconds, array $bookingItems = [])
    {
        Assert::that($supplierId)->notEmpty('Supplier Id cannot be empty');
        Assert::that($holdDurationSeconds)->greaterThan(0, 'Hold duration should be more than 0');
        $this->supplierId = $supplierId;
        $this->holdDurationSeconds = $holdDurationSeconds;
        $this->bookingItems = $bookingItems;
    }

    public function addBookingItem(BookingItem $bookingItem)
    {
        $this->bookingItems[] = $bookingItem;
    }

    public function generate()
    {
        Assert::that($this->bookingItems)->notEmpty('Booking Items cannot be empty')->all()->isInstanceOf(BookingItem::class, 'Incorrect Booking Item');
        return [
            'supplierId' => $this->supplierId,
            'bookingItems' => $this->getBookingItemsAsArray(),
            'holdDurationSeconds' => (int)$this->holdDurationSeconds
        ];
    }

    private function getBookingItemsAsArray()
    {
        $bookingItems = [];
        /** @var $bookingItem BookingItem*/
        foreach ($this->bookingItems as $bookingItem) {
            $bookingItems[] = $bookingItem->generate();
        }
        return $bookingItems;
    }
}