<?php

namespace TrekkConnect\Sdk\ApiClient\Methods;

use TrekkConnect\Sdk\ApiClient\Http\Response;
use TrekkConnect\Sdk\ApiClient\Methods\Activity\Query;

final class Category extends MethodsCollection
{
    public function find(Query $query)
    {
        return $this->request('category.find', []);
    }
}
