<?php

namespace TrekkConnect\Sdk\ApiClient\Methods;

use TrekkConnect\Sdk\ApiClient\Http\Response;

final class Ticket extends MethodsCollection
{
    /**
     * @param array $params
     *
     * @return Response
     */
    public function affected(array $params)
    {
        return $this->request('ticket.affected', $params);
    }
}
